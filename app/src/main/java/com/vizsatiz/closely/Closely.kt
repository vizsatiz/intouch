package com.vizsatiz.closely

import android.app.Application
import com.vizsatiz.calling.di.CallComponent
import com.vizsatiz.calling.di.DaggerCallComponent
import com.vizsatiz.common.di.CommonComponent
import com.vizsatiz.common.di.DaggerCommonComponent
import com.vizsatiz.datastore.di.component.DaggerDataStoreComponent
import com.vizsatiz.datastore.di.component.DataStoreComponent
import com.vizsatiz.closely.di.AppComponent
import com.vizsatiz.closely.di.DaggerAppComponent
import com.vizsatiz.closely.platform.EnvironmentUtilImpl
import com.vizsatiz.common.di.module.CommonModule
import com.vizsatiz.common.di.module.PlatformModule
import com.vizsatiz.datastore.di.module.DbModule

class Closely: Application() {

    private val mDataStoreComponent: DataStoreComponent = DaggerDataStoreComponent.builder()
        .dbModule(DbModule(this))
        .build()

    private val mCommonComponent: CommonComponent = DaggerCommonComponent.builder()
        .dataStoreComponent(mDataStoreComponent)
        .commonModule(CommonModule(this))
        .platformModule(PlatformModule(EnvironmentUtilImpl()))
        .build()

    private val mCallComponent: CallComponent = DaggerCallComponent.builder()
        .commonComponent(mCommonComponent)
        .dataStoreComponent(mDataStoreComponent)
        .build()

    private val mAppComponent: AppComponent = DaggerAppComponent.builder()
        .callComponent(mCallComponent)
        .commonComponent(mCommonComponent)
        .build()

    override fun onCreate() {
        super.onCreate()

        AppComponent.instance = mAppComponent
    }

}