package com.vizsatiz.closely.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.vizsatiz.closely.R
import com.vizsatiz.closely.model.UiContact
import com.vizsatiz.closely.vh.FriendSelectionViewHolder

class FriendSelectionAdapter(
    private val activityContext: Context
) : PagedListAdapter<UiContact, FriendSelectionViewHolder>(REPO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendSelectionViewHolder {
        val itemView = LayoutInflater.from(activityContext).inflate(
            R.layout.item_friend_selection_card,
            parent, false
        )
        return FriendSelectionViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FriendSelectionViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.bind(it)
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<UiContact>() {
            override fun areItemsTheSame(oldItem: UiContact, newItem: UiContact): Boolean =
                oldItem.number == newItem.number && oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: UiContact, newItem: UiContact): Boolean =
                oldItem == newItem
        }
    }
}