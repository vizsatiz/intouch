package com.vizsatiz.closely.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vizsatiz.closely.R
import com.vizsatiz.closely.model.UiContact
import com.vizsatiz.closely.vh.AllFriendsViewHolder

class MyFriendsAdapter(
    private val activityContext: Context
) : RecyclerView.Adapter<AllFriendsViewHolder>() {

    private val data: MutableList<UiContact> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllFriendsViewHolder {
        val itemView = LayoutInflater.from(activityContext).inflate(
            R.layout.item_my_friend_card,
            parent, false
        )
        return AllFriendsViewHolder(itemView)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: AllFriendsViewHolder, position: Int) {
        holder.bind(data[position])
    }

    fun setData(data: List<UiContact>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }
}