package com.vizsatiz.closely.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vizsatiz.closely.fragment.OnBoardingHomeFragment
import com.vizsatiz.closely.fragment.OnBoardingPermissionFragment
import com.vizsatiz.closely.fragment.OnBoardingWelcomeFragment
import java.lang.IllegalStateException

class OnBoardingVpAdapter(
    fa: FragmentActivity,
    private val onNextClick: (itemCount: Int) -> Unit,
    private val onBackClick: (itemCount: Int) -> Unit
) : FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment =
        when (position) {
            0 -> {
                OnBoardingHomeFragment.newInstance(
                    onNextClick = { onNextClick(itemCount) }
                )
            }
            1 -> {
                OnBoardingPermissionFragment.newInstance(
                    onNextClick = { onNextClick(itemCount) },
                    onBackClick = { onBackClick(itemCount) }
                )
            }
            2 -> {
                OnBoardingWelcomeFragment.newInstance(
                    onBackClick = { onBackClick(itemCount) }
                )
            }
            else -> {
                throw IllegalStateException("OnBoardingVpAdapter cannot deliver position: $position")
            }
        }
}
