package com.vizsatiz.closely.vh

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vizsatiz.closely.databinding.ItemFriendSelectionCardBinding
import com.vizsatiz.closely.model.UiContact

class FriendSelectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemFriendSelectionCardBinding.bind(itemView)

    fun bind(contact: UiContact) {
        binding.txtName.text = contact.name
        binding.txtNumber.text = contact.number
        binding.avatarImage.letter = contact.name

        binding.itemCard.setOnClickListener {
            binding.itemCard.let { it.isChecked = !it.isChecked }
        }
    }
}