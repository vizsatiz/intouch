package com.vizsatiz.closely.vh

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vizsatiz.closely.databinding.ItemMyFriendCardBinding
import com.vizsatiz.closely.model.UiContact

class AllFriendsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemMyFriendCardBinding.bind(itemView)

    fun bind(contact: UiContact) {
        binding.txtName.text = contact.name
        binding.txtNo.text = contact.number
        binding.avatarImage.letter = contact.name

        binding.itemCard.setOnClickListener {
            binding.itemCard.let { it.isChecked = !it.isChecked }
        }
    }
}