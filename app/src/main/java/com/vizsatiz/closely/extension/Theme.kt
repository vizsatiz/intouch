package com.vizsatiz.closely.extension

import android.app.Activity
import android.content.Context
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import com.vizsatiz.closely.R

fun Activity.getNormalThemedInflator(
    inflater: LayoutInflater
): LayoutInflater? {
    val contextThemeWrapper: Context = ContextThemeWrapper(this, R.style.AppTheme_NoActionBar_NormalTheme)
    return inflater.cloneInContext(contextThemeWrapper)
}