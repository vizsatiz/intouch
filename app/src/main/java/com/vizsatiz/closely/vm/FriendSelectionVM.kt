package com.vizsatiz.closely.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.vizsatiz.calling.repository.ContactRepo
import com.vizsatiz.closely.di.AppComponent
import com.vizsatiz.closely.model.UiContact
import com.vizsatiz.closely.model.toUiContact
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Named
import kotlin.coroutines.CoroutineContext

private const val PAGE_SIZE = 25
private const val INITIAL_LOAD_SIZE_HINT = 50

class FriendSelectionVM : ViewModel() {

    @Inject
    lateinit var contactsRepo: ContactRepo

    @field:[Inject Named("IO")]
    lateinit var ioContext: CoroutineContext

    init {
        AppComponent.instance.inject(this)
    }

    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .setInitialLoadSizeHint(INITIAL_LOAD_SIZE_HINT)
        .setPageSize(PAGE_SIZE)
        .build()

    private val unSuggestedDs = contactsRepo.getUnSuggestedContacts().map {
        it.toUiContact()
    }

    val unSuggestedContacts =  LivePagedListBuilder(unSuggestedDs, pagedListConfig)
        .build()

    val suggestedContacts: LiveData<List<UiContact>> =
        contactsRepo.getContactSuggestions().map { cntList ->
            cntList.map { cnt -> cnt.toUiContact() }
        }.asLiveData(ioContext)
}