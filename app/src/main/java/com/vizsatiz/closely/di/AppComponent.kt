package com.vizsatiz.closely.di

import com.vizsatiz.calling.di.CallComponent
import com.vizsatiz.closely.activity.AppHomeActivity
import com.vizsatiz.closely.activity.FriendSelectionActivity
import com.vizsatiz.closely.activity.OnBoardingActivity
import com.vizsatiz.closely.vm.FriendSelectionVM
import com.vizsatiz.common.di.CommonComponent
import dagger.Component

@AppScope
@Component(
    dependencies = [
        CallComponent::class,
        CommonComponent::class
    ],
    modules = [
    ]
)
interface AppComponent {

    companion object {
        lateinit var instance: AppComponent
    }

    fun inject(friendSelectionActivity: FriendSelectionActivity)
    fun inject(appHomeActivity: AppHomeActivity)
    fun inject(onBoardingActivity: OnBoardingActivity)
    fun inject(friendSelectionVM: FriendSelectionVM)
}