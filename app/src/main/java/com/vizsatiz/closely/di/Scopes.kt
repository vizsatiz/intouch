package com.vizsatiz.closely.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
internal annotation class AppScope