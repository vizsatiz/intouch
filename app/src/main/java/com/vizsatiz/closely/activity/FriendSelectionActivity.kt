package com.vizsatiz.closely.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vizsatiz.calling.repository.ContactRepo
import com.vizsatiz.calling.sync.SyncContext
import com.vizsatiz.calling.sync.SyncEngine
import com.vizsatiz.calling.sync.SyncObserver
import com.vizsatiz.closely.R
import com.vizsatiz.closely.adapter.FriendSelectionAdapter
import com.vizsatiz.closely.adapter.FriendSuggestionAdapter
import com.vizsatiz.closely.databinding.ActivityFriendsSelectionBinding
import com.vizsatiz.closely.di.AppComponent
import com.vizsatiz.closely.vm.FriendSelectionVM
import com.vizsatiz.common.platform.EnvironmentUtil
import javax.inject.Inject

class FriendSelectionActivity : AppCompatActivity(), SyncObserver {

    private val vm: FriendSelectionVM by lazy {
        ViewModelProvider(this).get(FriendSelectionVM::class.java)
    }

    @Inject
    lateinit var syncEngine: SyncEngine

    @Inject
    lateinit var contactsRepo: ContactRepo

    @Inject
    lateinit var environmentUtil: EnvironmentUtil

    private lateinit var binding: ActivityFriendsSelectionBinding

    private lateinit var  suggestionsAdapter: FriendSuggestionAdapter

    private lateinit var  unSuggestedAdapter: FriendSelectionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme_NoActionBar_NormalTheme)
        binding = ActivityFriendsSelectionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppComponent.instance
            .inject(this)

        setUpSuggestedRv()
        setUpUnSuggestedRv()

        syncEngine.registerSyncObserver(this)
        syncEngine.sync()

        binding.fab.setOnClickListener {
            navigateToAppHome()
        }
    }

    private fun setUpSuggestedRv() {
        suggestionsAdapter = FriendSuggestionAdapter(this)
        vm.suggestedContacts.observe(this@FriendSelectionActivity, Observer { contacts ->
            suggestionsAdapter.setData(contacts)
        })

        val suggestedRv: RecyclerView = binding.suggestedContacts
        suggestedRv.layoutManager = GridLayoutManager(this, 2)
        suggestedRv.itemAnimator = DefaultItemAnimator()
        suggestedRv.adapter = suggestionsAdapter
    }

    private fun setUpUnSuggestedRv() {
        unSuggestedAdapter = FriendSelectionAdapter(this)
        vm.unSuggestedContacts.observe(this@FriendSelectionActivity, Observer { contacts ->
            unSuggestedAdapter.submitList(contacts)
        })
        val unSuggestedRv: RecyclerView = binding.allContacts
        unSuggestedRv.layoutManager = LinearLayoutManager(this)
        unSuggestedRv.itemAnimator = DefaultItemAnimator()
        unSuggestedRv.adapter = unSuggestedAdapter
    }

    private fun navigateToAppHome() {
        startActivity(Intent(this@FriendSelectionActivity, AppHomeActivity::class.java))
    }

    override fun channelSyncCallback(context: SyncContext) {
        contactsRepo.markContactsAsSuggested()
    }
}
