package com.vizsatiz.closely.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.vizsatiz.calling.repository.ContactRepo
import com.vizsatiz.calling.sync.SyncContext
import com.vizsatiz.calling.sync.SyncEngine
import com.vizsatiz.calling.sync.SyncObserver
import com.vizsatiz.closely.R
import com.vizsatiz.closely.adapter.OnBoardingVpAdapter
import com.vizsatiz.closely.databinding.ActivityOnBoardingBinding
import com.vizsatiz.closely.di.AppComponent
import com.vizsatiz.common.platform.PermissionUtil
import javax.inject.Inject

class OnBoardingActivity : AppCompatActivity(), SyncObserver {

    private lateinit var binding: ActivityOnBoardingBinding

    @Inject
    lateinit var syncEngine: SyncEngine

    @Inject
    lateinit var contactsRepo: ContactRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme_NoActionBar_NormalTheme)
        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppComponent
            .instance.inject(this)

        startSyncIfPermissions()
        val vp = binding.viewPager2
        val adapter = OnBoardingVpAdapter(this, onNextClick =  { itemCount ->
            val currentItem = vp.currentItem
            vp.currentItem = if (currentItem < itemCount - 1) currentItem + 1 else currentItem
        }, onBackClick = {
            val currentItem = vp.currentItem
            vp.currentItem = if (currentItem > 0) currentItem - 1 else currentItem
        })

        vp.adapter = adapter
        TabLayoutMediator(binding.tabLayout, vp) { _, _ ->

        }.attach()
    }

    private fun startSyncIfPermissions() {
        if (PermissionUtil.hasCallAncContactConsent(this.applicationContext)) {
            syncEngine.registerSyncObserver(this)
            syncEngine.sync()
        }
    }

    // This needs to be here for permission fragment to work
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun channelSyncCallback(context: SyncContext) {
        if (context.isSyncCompleted())
            contactsRepo.markContactsAsSuggested()
    }

    override fun onDestroy() {
        syncEngine.registerSyncObserver(this)
        super.onDestroy()
    }
}