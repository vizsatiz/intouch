package com.vizsatiz.closely.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vizsatiz.calling.sync.SyncEngine
import com.vizsatiz.common.platform.PermissionUtil
import javax.inject.Inject

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this@SplashScreenActivity, OnBoardingActivity::class.java))
        finish()
    }
}