package com.vizsatiz.closely.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vizsatiz.closely.R
import com.vizsatiz.closely.adapter.MyFriendsAdapter
import com.vizsatiz.closely.databinding.ActivityAppHomeActivtyBinding
import com.vizsatiz.closely.di.AppComponent
import com.vizsatiz.closely.vm.FriendSelectionVM
import com.vizsatiz.common.platform.EnvironmentUtil
import javax.inject.Inject

class AppHomeActivity : AppCompatActivity() {

    private val vm: FriendSelectionVM by lazy {
        ViewModelProvider(this).get(FriendSelectionVM::class.java)
    }

    @Inject
    lateinit var environmentUtil: EnvironmentUtil

    private lateinit var binding: ActivityAppHomeActivtyBinding

    private lateinit var  rvAdapter: MyFriendsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme_NoActionBar_NormalTheme)
        binding = ActivityAppHomeActivtyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppComponent.instance
            .inject(this)

        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        rvAdapter = MyFriendsAdapter( this)
        vm.suggestedContacts.observe(this, Observer { contacts ->
            rvAdapter.setData(contacts)
        })

        val rv: RecyclerView = binding.myFriends
        rv.layoutManager = GridLayoutManager(this, 2)
        rv.itemAnimator = DefaultItemAnimator()
        rv.adapter = rvAdapter
    }
}