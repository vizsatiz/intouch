package com.vizsatiz.closely.qa

import com.vizsatiz.closely.model.UiContact

object MockedContactData {
    val suggestedContact = listOf(
        UiContact("Vishnu Satis", "96181347123"),
        UiContact("Sabz", "961813899"),
        UiContact("Vishnu Satis", "96181347123"),
        UiContact("Sabz", "961813899")
    )
}