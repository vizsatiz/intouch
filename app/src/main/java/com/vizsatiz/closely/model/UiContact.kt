package com.vizsatiz.closely.model

import com.vizsatiz.datastore.models.Contact

data class UiContact(val name: String, val number: String)

fun Contact.toUiContact() = UiContact(this.name, this.number)