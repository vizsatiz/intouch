package com.vizsatiz.closely.platform

import com.vizsatiz.closely.BuildConfig
import com.vizsatiz.common.platform.EnvironmentUtil

class EnvironmentUtilImpl: EnvironmentUtil {

    override fun isDebugFeaturesEnabled(): Boolean {
        return BuildConfig.DEBUG
    }

    override fun isDataMocksEnabled(): Boolean {
        return BuildConfig.MOCK_DATA
    }
}