package com.vizsatiz.closely.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vizsatiz.closely.databinding.OnBoardingHomeFragmentBinding
import com.vizsatiz.closely.extension.getNormalThemedInflator

class OnBoardingHomeFragment(private val onNextClick: () -> Unit): Fragment() {

    private var _binding: OnBoardingHomeFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.nextChip.setOnClickListener { onNextClick() }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.activity?.getNormalThemedInflator(inflater)?.let {
            _binding = OnBoardingHomeFragmentBinding.inflate(it, container, false)
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance(onNextClick: () -> Unit): OnBoardingHomeFragment {
            return OnBoardingHomeFragment(onNextClick)
        }
    }
}