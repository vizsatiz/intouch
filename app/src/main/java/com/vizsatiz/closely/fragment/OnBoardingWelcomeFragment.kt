package com.vizsatiz.closely.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vizsatiz.closely.activity.FriendSelectionActivity
import com.vizsatiz.closely.databinding.OnBoardingWelcomeFragmentBinding
import com.vizsatiz.closely.extension.getNormalThemedInflator

class OnBoardingWelcomeFragment(
    private val onBackClick: () -> Unit
): Fragment() {

    private var _binding: OnBoardingWelcomeFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.backBtn.setOnClickListener { onBackClick() }
        binding.welcomeBtn.setOnClickListener {
            startActivity(Intent(this@OnBoardingWelcomeFragment.activity, FriendSelectionActivity::class.java))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.activity?.getNormalThemedInflator(inflater)?.let {
            _binding = OnBoardingWelcomeFragmentBinding.inflate(it, container, false)
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance(onBackClick: () -> Unit): OnBoardingWelcomeFragment {
            return OnBoardingWelcomeFragment(onBackClick)
        }
    }
}