package com.vizsatiz.closely.fragment

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.vizsatiz.closely.databinding.OnBoardingPermissionFragmentBinding
import com.vizsatiz.closely.extension.getNormalThemedInflator
import com.vizsatiz.common.LogUtil
import com.vizsatiz.common.platform.PermissionUtil

class OnBoardingPermissionFragment(
    private val onNextClick: () -> Unit,
    private val onBackClick: () -> Unit
): Fragment() {

    private var _binding: OnBoardingPermissionFragmentBinding? = null
    private val binding get() = _binding!!


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.permitChip.setOnClickListener {
            if(!checkForPermissions())
                onNextClick()
        }

        binding.backBtn.setOnClickListener {
            onBackClick()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionUtil.PERMISSION_REQUEST_CONTACT_AND_CALL_READ -> {
                check(permissions.size == 2) { "We have asked for 2 permissions" }
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                    onNextClick()
                else
                    "Not all permissions were granted".showSnackBar(Snackbar.LENGTH_LONG)
            }
            else -> "Not all permissions were granted".showSnackBar(Snackbar.LENGTH_LONG)
        }
    }

    private fun String.showSnackBar(time: Int) =  Snackbar.make(binding.permissionPage, this, time).show()

    private fun checkForPermissions(): Boolean {
        activity?.applicationContext?.let {
            return if (!PermissionUtil.hasCallLogReadConsent(it) || !PermissionUtil.hasContactReadConsent(it)) {
                requestPermissions(
                    arrayOf(android.Manifest.permission.READ_CONTACTS, android.Manifest.permission.READ_CALL_LOG),
                    PermissionUtil.PERMISSION_REQUEST_CONTACT_AND_CALL_READ
                )
                true
            } else {
                LogUtil.d("We have all permissions, nothing to ask for")
                false
            }
        }
        return false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.activity?.getNormalThemedInflator(inflater)?.let {
            _binding = OnBoardingPermissionFragmentBinding.inflate(it, container, false)
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance(
            onNextClick: () -> Unit,
            onBackClick: () -> Unit
        ): OnBoardingPermissionFragment {
            return OnBoardingPermissionFragment(onNextClick, onBackClick)
        }
    }
}