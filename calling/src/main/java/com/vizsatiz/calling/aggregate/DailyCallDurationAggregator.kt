package com.vizsatiz.calling.aggregate

import com.vizsatiz.datastore.models.HomeGraphModel
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import javax.inject.Inject

data class AggregatedGraphModel(
    val number: String,
    val day: String,
    val duration: Float
)

interface DailyCallDurationAggregator {
    fun aggregateCallRateByDay(callCounts: List<HomeGraphModel>): List<AggregatedGraphModel>
    fun generateCallRateForDays(callCounts: List<HomeGraphModel>, number: String, noODays: Int): List<HomeGraphModel>
}

class DailyCallDurationAggregatorImpl @Inject constructor() : DailyCallDurationAggregator {

    override fun generateCallRateForDays(
        callCounts: List<HomeGraphModel>,
        number: String,
        noODays: Int
    ): List<HomeGraphModel> {
        val generatedDates = generateDateForLastNDays(noODays)
        val mapByDay = callCounts.associate { it.day to it.duration }
        return generatedDates.map {
            HomeGraphModel(
                number = number,
                day = it,
                duration = mapByDay[it] ?: 0
            )
        }
    }

    override fun aggregateCallRateByDay(callCounts: List<HomeGraphModel>): List<AggregatedGraphModel> =
        if (callCounts.isNotEmpty()) {
            var aPrev = 0.0F
            callCounts.mapIndexed { index, it ->
                val prevSum = aPrev * (if (index > 0) index else 1)
                val currentValue = (prevSum + it.duration.toFloat()) / (index.toFloat() + 1.0F)
                aPrev = currentValue
                AggregatedGraphModel(it.number, it.day, currentValue)
            }
        } else emptyList()

    fun generateDateForLastNDays(n: Int): List<String> {
        val today = DateTime()
        val dtf = DateTimeFormat.forPattern("dd/MM/yyyy")
        return (0 until n).map {
            dtf.print(today.minusDays(it))
        }
    }
}