package com.vizsatiz.calling.sync

import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

interface SyncContext {

    fun registerSyncStart(sessionName: String)
    fun registerSyncError(sessionName: String)
    fun registerSyncSuccess(sessionName: String)
    fun isSyncInProgress(sessionName: String): Boolean
    fun isSyncInProgress(): Boolean
    fun resetSyncStates()
    fun isSyncCompleted(): Boolean

    sealed class Status {
        object InProgress : Status()
        object Success : Status()
        object Error : Status()
    }
}

class SyncContextImpl @Inject constructor() : SyncContext {

    private val sessions = ConcurrentHashMap<String, SyncContext.Status>()

    override fun registerSyncStart(sessionName: String) {
        sessions[sessionName] = SyncContext.Status.InProgress
    }

    override fun registerSyncError(sessionName: String) {
        sessions[sessionName] = SyncContext.Status.Error
    }

    override fun registerSyncSuccess(sessionName: String) {
        sessions[sessionName] = SyncContext.Status.Success
    }

    override fun isSyncInProgress(): Boolean = sessions.isNotEmpty() && sessions.values.contains(SyncContext.Status.InProgress)

    override fun isSyncCompleted(): Boolean = sessions.isNotEmpty() && !sessions.values.contains(SyncContext.Status.InProgress)

    override fun resetSyncStates() = sessions.clear()

    override fun isSyncInProgress(sessionName: String): Boolean = sessions[sessionName]?.let {
        it == SyncContext.Status.InProgress
    } ?: false
}