package com.vizsatiz.calling.sync

enum class ContentOperation {
    INSERT,
    UPDATE,
    DELETE
}