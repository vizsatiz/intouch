package com.vizsatiz.calling.sync

import com.vizsatiz.common.LogUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named
import kotlin.coroutines.CoroutineContext

interface SyncEngine {
    fun sync()
    fun shutdown()
    fun registerSyncObserver(observer: SyncObserver)
    fun removeSyncObserver(observer: SyncObserver)
}

class SyncEngineImpl @Inject constructor(
    @Named("IO") private val ioCoroutineContext: CoroutineContext,
    provider: SyncProvider,
    private val syncContext: SyncContext
) : SyncEngine {

    private val channels = provider.getSyncableChannels()
    private val job = SupervisorJob()

    private val scope = CoroutineScope(ioCoroutineContext + job)

    init {
        channels.forEach {
            it.registerNativeContentObserver { sync() }
        }
    }

    override fun sync() {
        if (!syncContext.isSyncInProgress()) {
            syncContext.resetSyncStates()
            channels.forEach {
                scope.launch { it.sync() }
            }
        }
    }

    override fun shutdown() {
        job.run { cancelChildren() }
        scope.launch {
            channels.forEach { it.close() }
        }
        LogUtil.d("SyncEngine is shutdown", "SyncEngine")
    }

    override fun registerSyncObserver(observer: SyncObserver) {
        channels.forEach { it.registerSyncObserver(observer) }
    }

    override fun removeSyncObserver(observer: SyncObserver) {
        channels.forEach { it.removeSyncObserver(observer) }
    }
}