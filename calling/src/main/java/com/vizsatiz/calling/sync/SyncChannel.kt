package com.vizsatiz.calling.sync

import com.vizsatiz.calling.di.module.CALL_LOG_CHANNEL
import com.vizsatiz.calling.di.module.CONTACT_CHANNEL
import com.vizsatiz.common.LogUtil
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import javax.inject.Inject
import javax.inject.Named
import kotlin.system.measureTimeMillis

interface SyncObserver {
    fun channelSyncCallback(context: SyncContext)
}
open class Syncable
abstract class SyncChannel<T : Syncable> constructor(private val syncContext: SyncContext) {

    private val syncObservers: MutableList<SyncObserver> = mutableListOf()

    abstract fun name(): String
    abstract fun registerNativeContentObserver(callback: () -> Unit)
    abstract suspend fun source(): Flow<T>
    abstract suspend fun sink(v: T)
    abstract suspend fun close()

    suspend fun sync() {
        if (!shouldStartSync()) {
            try {
                syncContext.registerSyncStart(name())
                LogUtil.d("Starting sync of ${name()}")
                val syncFlow = source()
                val time = measureTimeMillis {
                    syncFlow.collect { value ->
                        sink(value)
                    }
                }
                LogUtil.d("Finishing sync of ${name()} in: $time ms")
                close()
                syncContext.registerSyncSuccess(name())
            } catch (e: Exception) {
                syncContext.registerSyncError(name())
            }
        } else {
            LogUtil.d("Skipping sync for ${name()}")
        }
        syncObservers.forEach { it.channelSyncCallback(syncContext) }
    }

    fun registerSyncObserver(observer: SyncObserver) {
        syncObservers.add(observer)
    }

    fun removeSyncObserver(observer: SyncObserver) {
        syncObservers.remove(observer)
    }

    private fun shouldStartSync() = syncContext.isSyncInProgress(name())
}

interface SyncProvider {
    fun getSyncableChannels(): Set<SyncChannel<*>>
}

class SyncProviderImpl @Inject constructor(
    @Named(CALL_LOG_CHANNEL) private val callLogsChannel: SyncChannel<RawCallData>,
    @Named(CONTACT_CHANNEL) private val contactChannel: SyncChannel<RawContact>
) : SyncProvider {
    override fun getSyncableChannels(): Set<SyncChannel<*>> = setOf(
        contactChannel,
        callLogsChannel
    )
}