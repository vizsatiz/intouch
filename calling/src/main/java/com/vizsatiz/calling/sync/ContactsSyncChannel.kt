package com.vizsatiz.calling.sync

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.database.Cursor
import android.provider.ContactsContract
import androidx.core.app.ActivityCompat
import com.vizsatiz.calling.repository.SyncRepo
import com.vizsatiz.common.LogUtil
import com.vizsatiz.common.platform.PermissionUtil
import com.vizsatiz.common.state.AppStateUtil
import com.vizsatiz.datastore.models.Contact
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.Date
import javax.inject.Inject

data class RawContact(
    val id: String,
    val name: String,
    val number: String,
    val normNumber: String,
    val lastUpdated: String
) : Syncable() {
    fun toContact() = Contact(
        name = name,
        rawId = id,
        number = number,
        normNumber = normNumber,
        lastUpdated = Date(lastUpdated.toLong())
    )
}

private const val CONTACTS_SYNC_BATCH = 200

class ContactsSyncChannel @Inject constructor(
    private val context: Context,
    syncContext: SyncContext,
    private val appStateUtil: AppStateUtil,
    private val syncRepo: SyncRepo
) : SyncChannel<RawContact>(syncContext) {

    private var contentCursor: Cursor? = null
    private val contactBatchCache = mutableListOf<Contact>()

    override fun name(): String = "CONTACTS"

    override fun registerNativeContentObserver(callback: () -> Unit) {
        if(PermissionUtil.hasCallLogReadConsent(context)) {
            context.contentResolver
                .registerContentObserver(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, true, object : ContentObserver(null) {
                    override fun onChange(selfChange: Boolean) {
                        super.onChange(selfChange)
                        // TODO work with deletes
                        callback()
                    }
                })
        }
    }

    override suspend fun source(): Flow<RawContact> {
        validateSession()
        contentCursor = getContactCursor(getLastSyncState())
        return flow {
            contentCursor?.let { cur ->
                while (cur.moveToNext()) {
                    val id: String = cur.getString(
                        cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)
                    )
                    val name: String = cur.getString(
                        cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                    )
                    val phoneNo: String = cur.getString(
                        cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                    )
                    val normalizedNo: String? = cur.getString(
                        cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
                    )
                    val updatedAt: String = cur.getString(
                        cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP)
                    )
                    normalizedNo?.let { emit(RawContact(id, name, phoneNo, it, updatedAt)) }
                }
            }
        }
    }

    override suspend fun sink(v: RawContact) {
        contactBatchCache.add(v.toContact())
        if (contactBatchCache.size == CONTACTS_SYNC_BATCH) {
            commitContactBatch()
        }
    }

    private suspend fun commitContactBatch() {
        if (contactBatchCache.isNotEmpty()) {
            syncRepo.insertContacts(contactBatchCache)
            updateLatestState(
                contactBatchCache
                    .last().lastUpdated.time
            )
            LogUtil.d("Contacts committed, count: ${contactBatchCache.size}", "ContactResolver")
            contactBatchCache.clear()
        } else {
            LogUtil.d("No more ${name()} to commit", "ContactResolver")
        }
    }

    private suspend fun updateLatestState(date: Long) {
        appStateUtil.updateStateByOwner(AppStateUtil.STATE_OWNER_CONTACT_SYNC, date.toString())
    }

    override suspend fun close() {
        commitContactBatch()
        contentCursor?.close()
        contentCursor = null
    }

    private suspend fun getLastSyncState(): Date = Date(
        appStateUtil
            .getStateByOwner(AppStateUtil.STATE_OWNER_CONTACT_SYNC)
            .value.toLong()
    )

    private fun validateSession() {
        contentCursor?.let { throw IllegalArgumentException("Trying to source without closing the previous session") }
        if (contactBatchCache.size > 0) {
            throw IllegalStateException("Last sync session was terminated unexpectedly for contacts")
        }
        if (!PermissionUtil.hasCallLogReadConsent(context)) {
            throw IllegalStateException("Trying to access resource without permission: ${name()}")
        }
    }

    @SuppressLint("Recycle")
    private fun getContactCursor(fromDate: Date): Cursor? {
        val projections = listOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
            ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP
        ).toTypedArray()
        return if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            context.contentResolver
                .query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    projections,
                    constructTimeClause(),
                    listOf(fromDate.time.toString()).toTypedArray(),
                    null
                )
        } else throw IllegalStateException("Trying to access contact common kind without consent")
    }

    private fun constructTimeClause() = "${ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP} > ?"

    private fun getContentOperation(): ContentOperation {
        val currentLocalSize = 0
        val remoteSize = getContactCursor(Date(0))?.count ?: 0
        return when {
            currentLocalSize > remoteSize -> ContentOperation.DELETE
            currentLocalSize == remoteSize -> ContentOperation.UPDATE
            else -> ContentOperation.INSERT
        }
    }
}