package com.vizsatiz.calling.sync

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.database.Cursor
import android.provider.CallLog
import androidx.core.app.ActivityCompat
import com.vizsatiz.calling.repository.SyncRepo
import com.vizsatiz.common.LogUtil
import com.vizsatiz.common.platform.PermissionUtil
import com.vizsatiz.common.state.AppStateUtil
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.Date
import javax.inject.Inject

private const val CALL_SYNC_BATCH_SIZE = 200
data class RawCallData(
    val number: String,
    val normalizedNo: String?,
    val name: String?,
    val date: String,
    val type: String,
    val duration: String
): Syncable() {
    fun toCallLog(): com.vizsatiz.datastore.models.CallLog = com.vizsatiz.datastore.models.CallLog(
        this.number,
        this.normalizedNo,
        this.name,
        Date(this.date.toLong()),
        this.type,
        this.duration.toLong()
    )
}

class CallLogsSyncChannel @Inject constructor(
    private val context: Context,
    syncContext: SyncContext,
    private val appStateUtil: AppStateUtil,
    private val syncRepo: SyncRepo
) : SyncChannel<RawCallData>(syncContext) {

    private var contentCursor: Cursor? = null
    private val callLogBatchCache = mutableListOf<com.vizsatiz.datastore.models.CallLog>()

    override fun name() = "CALL_LOGS"

    override suspend fun source(): Flow<RawCallData> {
        validateSession()
        contentCursor = getCallLogCursor(getLastSyncState())
        return flow {
            contentCursor?.let { cur ->
                while (cur.moveToNext()) {
                    val callNumber: String = cur.getString(cur.getColumnIndex(CallLog.Calls.NUMBER))
                    val callName: String? = cur.getString(cur.getColumnIndex(CallLog.Calls.CACHED_NAME))
                    val callDate: String = cur.getString(cur.getColumnIndex(CallLog.Calls.DATE))
                    val callType: String = cur.getString(cur.getColumnIndex(CallLog.Calls.TYPE))
                    val duration: String = cur.getString(cur.getColumnIndex(CallLog.Calls.DURATION))
                    val normalisedNo: String? = cur.getString(cur.getColumnIndex(CallLog.Calls.CACHED_NORMALIZED_NUMBER))
                    callName?.let {
                        emit(RawCallData(callNumber, normalisedNo, callName, callDate, callType, duration))
                    }
                }
            }
        }
    }

    private fun validateSession() {
        contentCursor?.let { throw IllegalArgumentException("Trying to source without closing the previous session") }
        if (callLogBatchCache.size > 0 ) {
            throw IllegalStateException("Last sync session was terminated unexpectedly for call-logs")
        }
        if (!PermissionUtil.hasCallLogReadConsent(context)) {
            throw IllegalStateException("Trying to access resource without permission: ${name()}")
        }
    }

    override suspend fun sink(v: RawCallData) {
        callLogBatchCache.add(v.toCallLog())
        if (callLogBatchCache.size == CALL_SYNC_BATCH_SIZE) {
            commitCallLogBatch()
        }
    }

    override fun registerNativeContentObserver(callback: () -> Unit) {
        if(PermissionUtil.hasCallLogReadConsent(context)) {
            context.contentResolver
                .registerContentObserver(CallLog.Calls.CONTENT_URI, true, object : ContentObserver(null) {
                    override fun onChange(selfChange: Boolean) {
                        super.onChange(selfChange)
                        callback()
                    }
                })
        }
    }

    override suspend fun close() {
        commitCallLogBatch()
        contentCursor?.close()
        contentCursor = null
    }

    private suspend fun commitCallLogBatch() {
        if (callLogBatchCache.isNotEmpty()) {
            syncRepo.insertCallLogs(callLogBatchCache)
            updateLatestState(
                callLogBatchCache
                    .last().date.time
            )
            LogUtil.d("CallLog committed, count: ${callLogBatchCache.size}", "CallLogResolver")
            callLogBatchCache.clear()
        } else{
            LogUtil.d("No more ${name()} to commit", "CallLogResolver")
        }
    }

    private suspend fun updateLatestState(date: Long) {
        appStateUtil.updateStateByOwner(AppStateUtil.STATE_OWNER_CALL_LOG_SYNC, date.toString())
    }

    private suspend fun getLastSyncState(): Date = Date(
        appStateUtil
            .getStateByOwner(AppStateUtil.STATE_OWNER_CALL_LOG_SYNC)
            .value.toLong()
    )

    @SuppressLint("Recycle")
    private fun getCallLogCursor(fromDate: Date): Cursor? {
        val projection = listOf(
            CallLog.Calls.CACHED_NORMALIZED_NUMBER,
            CallLog.Calls.NUMBER,
            CallLog.Calls.CACHED_NAME,
            CallLog.Calls.DATE,
            CallLog.Calls.TYPE,
            CallLog.Calls.DURATION
        ).toTypedArray()
        return if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
            context.contentResolver
                .query(
                    CallLog.Calls.CONTENT_URI,
                    projection,
                    constructSelectionClause(),
                    arrayOf(
                        fromDate.time.toString()
                    ),
                    "${CallLog.Calls.LAST_MODIFIED} DESC"
                )
        } else throw IllegalStateException("Trying to access call logs without consent")
    }

    private fun constructSelectionClause() = "${CallLog.Calls.DATE} > ?"
}