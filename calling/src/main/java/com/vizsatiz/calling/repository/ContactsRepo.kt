package com.vizsatiz.calling.repository

import androidx.paging.DataSource
import com.vizsatiz.datastore.dao.CallDao
import com.vizsatiz.datastore.models.Contact
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

private const val SUGGESTED_CONTACTS_LIMIT = 4
interface ContactRepo {
    fun getContactSuggestions(): Flow<List<Contact>>
    fun getUnSuggestedContacts(): DataSource.Factory<Int, Contact>
    fun markContactsAsSuggested()
}

class ContactRepoImpl @Inject constructor(
    private val callDao: CallDao
): ContactRepo {

    override fun getContactSuggestions(): Flow<List<Contact>> {
        return callDao.getSuggestedContacts()
    }

    override fun getUnSuggestedContacts(): DataSource.Factory<Int, Contact> {
        return callDao.getUnSuggestedContacts()
    }

    override fun markContactsAsSuggested() {
        val avg = callDao.getAvgDurationGroupByNumber()
        avg?.let {
            val delta = avg.div(2.0)
            callDao.markSuggestedContacts(
                lower = it - delta,
                upper = it + delta,
                limit = SUGGESTED_CONTACTS_LIMIT
            )
        }
    }
}