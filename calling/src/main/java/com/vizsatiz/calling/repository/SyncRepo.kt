package com.vizsatiz.calling.repository

import com.vizsatiz.datastore.dao.CallDao
import com.vizsatiz.datastore.models.CallLog
import com.vizsatiz.datastore.models.Contact
import javax.inject.Inject

interface SyncRepo {
    suspend fun insertCallLogs(callLogs: List<CallLog>)
    suspend fun insertContacts(contacts: List<Contact>)
}

class SyncRepoImpl @Inject constructor(
    private val callDao: CallDao
): SyncRepo {

    override suspend fun insertCallLogs(callLogs: List<CallLog>)  = callDao.insertCallLogs(callLogs)

    override suspend fun insertContacts(contacts: List<Contact>) = callDao.insertContacts(contacts)

}