package com.vizsatiz.calling.di.module

import com.vizsatiz.calling.di.CallScope
import com.vizsatiz.calling.repository.ContactRepoImpl
import com.vizsatiz.calling.repository.ContactRepo
import dagger.Binds
import dagger.Module

@Module
interface FriendSelectModule {

    @Binds
    @CallScope
    fun getContactRepo(cntImpl: ContactRepoImpl): ContactRepo
}