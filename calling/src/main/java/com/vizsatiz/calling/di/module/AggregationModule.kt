package com.vizsatiz.calling.di.module

import com.vizsatiz.calling.aggregate.DailyCallDurationAggregator
import com.vizsatiz.calling.aggregate.DailyCallDurationAggregatorImpl
import com.vizsatiz.calling.di.CallScope
import dagger.Binds
import dagger.Module

@Module
interface AggregationModule {

    @Binds
    @CallScope
    fun getDailyCallDurationAggregator(callAggregator: DailyCallDurationAggregatorImpl): DailyCallDurationAggregator
}