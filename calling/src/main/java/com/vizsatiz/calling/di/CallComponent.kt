package com.vizsatiz.calling.di

import com.vizsatiz.calling.di.module.AggregationModule
import com.vizsatiz.calling.di.module.CallModule
import com.vizsatiz.calling.di.module.FriendSelectModule
import com.vizsatiz.calling.repository.ContactRepo
import com.vizsatiz.calling.repository.SyncRepo
import com.vizsatiz.calling.sync.SyncEngine
import com.vizsatiz.common.di.CommonComponent
import com.vizsatiz.datastore.di.component.DataStoreComponent
import dagger.Component

@Component(
    dependencies = [
        CommonComponent::class,
        DataStoreComponent::class
    ], modules = [
        CallModule::class,
        AggregationModule::class,
        FriendSelectModule::class
    ]
)
@CallScope
interface CallComponent {
    fun getSyncEngine(): SyncEngine

    fun getCallLogRepository(): SyncRepo
    fun getContactRepository(): ContactRepo
}