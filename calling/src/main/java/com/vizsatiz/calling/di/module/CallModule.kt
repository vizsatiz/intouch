package com.vizsatiz.calling.di.module

import com.vizsatiz.calling.di.CallScope
import com.vizsatiz.calling.repository.SyncRepo
import com.vizsatiz.calling.repository.SyncRepoImpl
import com.vizsatiz.calling.sync.CallLogsSyncChannel
import com.vizsatiz.calling.sync.ContactsSyncChannel
import com.vizsatiz.calling.sync.SyncEngine
import com.vizsatiz.calling.sync.SyncEngineImpl
import com.vizsatiz.calling.sync.RawCallData
import com.vizsatiz.calling.sync.RawContact
import com.vizsatiz.calling.sync.SyncProvider
import com.vizsatiz.calling.sync.SyncProviderImpl
import com.vizsatiz.calling.sync.SyncChannel
import com.vizsatiz.calling.sync.SyncContext
import com.vizsatiz.calling.sync.SyncContextImpl
import dagger.Binds
import dagger.Module
import javax.inject.Named

const val CALL_LOG_CHANNEL = "call_log_channel"
const val CONTACT_CHANNEL = "contact_channel"

@Module
interface CallModule {

    @Binds
    @CallScope
    fun getSyncEngine(callSyncEngine: SyncEngineImpl): SyncEngine

    @Binds
    @CallScope
    fun getCallLogRepo(callLogRepo: SyncRepoImpl): SyncRepo

    @Binds
    @CallScope
    @Named(CALL_LOG_CHANNEL)
    fun getCallLogSyncChannel(callResolver: CallLogsSyncChannel): SyncChannel<RawCallData>

    @Binds
    @CallScope
    @Named(CONTACT_CHANNEL)
    fun getContactSyncChannel(contactResolver: ContactsSyncChannel): SyncChannel<RawContact>

    @Binds
    @CallScope
    fun getSyncProvider(provider: SyncProviderImpl): SyncProvider

    @Binds
    @CallScope
    fun getSyncContext(syncContext: SyncContextImpl): SyncContext

}