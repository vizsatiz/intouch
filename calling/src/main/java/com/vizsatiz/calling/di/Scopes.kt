package com.vizsatiz.calling.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
internal annotation class CallScope