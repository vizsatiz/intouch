package com.vizsatiz.calling.aggregate

import com.vizsatiz.datastore.models.HomeGraphModel
import org.junit.Assert
import org.junit.Test

class DailyCallDurationAggregatorTest {

    @Test
    fun `aggregateCallRateByDay - with empty data - return empty result`() {
        val input = listOf<HomeGraphModel>()

        val dailyCallDurationAggregator = DailyCallDurationAggregatorImpl()

        val output = dailyCallDurationAggregator.aggregateCallRateByDay(input)
        Assert.assertTrue(output.isEmpty())
    }

    @Test
    fun `aggregateCallRateByDay - with valid data - return aggregated result`() {
        val input = listOf(
            HomeGraphModel("1234567", "Date()", 100),
            HomeGraphModel("123423", "Date()", 200)
        )

        val dailyCallDurationAggregator = DailyCallDurationAggregatorImpl()

        val output = dailyCallDurationAggregator.aggregateCallRateByDay(input)
        Assert.assertEquals(100.0F, output[0].duration)
        Assert.assertEquals(150.0F, output[1].duration)
    }

    @Test
    fun `aggregateCallRateByDay - with more valid data - return aggregated result`() {
        val input = listOf(
            HomeGraphModel("1234567", "Date()", 100),
            HomeGraphModel("123423", "Date()", 200),
            HomeGraphModel("1234232", "Date()", 500),
            HomeGraphModel("12322", "Date()", 700),
            HomeGraphModel("121003", "Date()", 1000)
        )

        val dailyCallDurationAggregator = DailyCallDurationAggregatorImpl()

        val output = dailyCallDurationAggregator.aggregateCallRateByDay(input)
        Assert.assertEquals(100.0F, output[0].duration)
        Assert.assertEquals(150.0F, output[1].duration)
        Assert.assertEquals(266.66666F, output[2].duration)
        Assert.assertEquals(375.0F, output[3].duration)
        Assert.assertEquals(500.0F, output[4].duration)
    }

    @Test
    fun `generateDateForLastNDays - for n = 0 - returns nothing`() {
        val dailyCallDurationAggregator = DailyCallDurationAggregatorImpl()
        val data = dailyCallDurationAggregator.generateDateForLastNDays(0)

        Assert.assertEquals(0, data.size)
    }

    @Test
    fun `generateDateForLastNDays - for n = 5 - returns nothing`() {
        val dailyCallDurationAggregator = DailyCallDurationAggregatorImpl()
        val data = dailyCallDurationAggregator.generateDateForLastNDays(5)

        Assert.assertEquals(5, data.size)
    }

    @Test
    fun `generateDateForLastNDays - for n = 60 - returns nothing`() {
        val dailyCallDurationAggregator = DailyCallDurationAggregatorImpl()
        val data = dailyCallDurationAggregator.generateDateForLastNDays(60)

        Assert.assertEquals(60, data.size)
    }

    @Test
    fun `generateCallRateForDays - with valid data - returns date wise data`() {
        val input = listOf(
            HomeGraphModel("1234567", "27/12/2020", 100),
            HomeGraphModel("123423", "25/12/2020", 200)
        )
        val dailyCallDurationAggregator = DailyCallDurationAggregatorImpl()
        val allData = dailyCallDurationAggregator
            .generateCallRateForDays(input, "1234", 5)

        Assert.assertEquals(5, allData.size)
        Assert.assertEquals(100, allData[0].duration)
        Assert.assertEquals(0, allData[1].duration)
        Assert.assertEquals(200, allData[2].duration)
    }
}