package com.vizsatiz.datastore.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vizsatiz.datastore.Converters
import com.vizsatiz.datastore.dao.CallDao
import com.vizsatiz.datastore.dao.UtilityDao
import com.vizsatiz.datastore.models.CallLog
import com.vizsatiz.datastore.models.Contact
import com.vizsatiz.datastore.models.State

@Database(
    entities = [
        State::class,
        CallLog::class,
        Contact::class
    ],
    version = 1
)
@TypeConverters(value = [Converters::class])
abstract class AppDb : RoomDatabase() {
    abstract fun utilityDao(): UtilityDao
    abstract fun callDao(): CallDao
}