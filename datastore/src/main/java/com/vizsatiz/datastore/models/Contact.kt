package com.vizsatiz.datastore.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.util.Date

@Entity(tableName = "contact", primaryKeys = ["name", "norm_number"])
data class Contact(
    val number: String,
    val name: String,
    @ColumnInfo(name = "norm_number")
    val normNumber: String,
    @ColumnInfo(name = "raw_id")
    val rawId: String,
    val suggested: Boolean = false,
    @ColumnInfo(name = "last_updated_ts")
    val lastUpdated: Date,
    @ColumnInfo(name = "created_at")
    val createdAt: Date = Date()
)