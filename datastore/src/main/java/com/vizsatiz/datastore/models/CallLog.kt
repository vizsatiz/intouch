package com.vizsatiz.datastore.models

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "call_log")
data class CallLog(
    val number: String,
    @ColumnInfo(name = "normalized_no")
    val normalizedNo: String?,
    val name: String?,
    val date: Date,
    val type: String,
    val duration: Long,
    @ColumnInfo(name = "created_at")
    val createdAt: Date = Date()
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

@Keep
data class HomeGraphModel(
    val number: String,
    val day: String,
    val duration: Long
)