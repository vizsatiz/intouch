package com.vizsatiz.datastore.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "state")
class State (
    @PrimaryKey
    @ColumnInfo(name = "owner_name")
    val ownerName: String,
    @ColumnInfo(name = "value")
    val value: String
)