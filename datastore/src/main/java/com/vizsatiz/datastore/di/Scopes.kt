package com.vizsatiz.datastore.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
internal annotation class DataStoreScope