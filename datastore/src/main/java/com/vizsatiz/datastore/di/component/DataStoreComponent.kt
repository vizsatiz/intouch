package com.vizsatiz.datastore.di.component

import com.vizsatiz.datastore.dao.CallDao
import com.vizsatiz.datastore.dao.UtilityDao
import com.vizsatiz.datastore.di.DataStoreScope
import com.vizsatiz.datastore.di.module.DbModule
import dagger.Component

@DataStoreScope
@Component(
    modules = [
        DbModule::class
    ]
)
interface DataStoreComponent {
    fun getUtilityDao(): UtilityDao
    fun getCallDao(): CallDao
}