package com.vizsatiz.datastore.di.module

import android.content.Context
import androidx.room.Room
import com.vizsatiz.datastore.dao.CallDao
import com.vizsatiz.datastore.dao.UtilityDao
import com.vizsatiz.datastore.db.AppDb
import com.vizsatiz.datastore.di.DataStoreScope
import dagger.Module
import dagger.Provides

@Module
class DbModule(private val applicationContext: Context) {

    @Provides
    @DataStoreScope
    fun createDb(): AppDb {
        return Room
            .databaseBuilder(applicationContext, AppDb::class.java, "Closely.db")
            .build()
    }

    @Provides
    @DataStoreScope
    fun getUtilityDao(db: AppDb): UtilityDao {
        return db.utilityDao()
    }

    @Provides
    @DataStoreScope
    fun getCallDao(db: AppDb): CallDao {
        return db.callDao()
    }

}