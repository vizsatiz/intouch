package com.vizsatiz.datastore.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vizsatiz.datastore.models.CallLog
import com.vizsatiz.datastore.models.Contact
import com.vizsatiz.datastore.models.HomeGraphModel
import kotlinx.coroutines.flow.Flow
import androidx.paging.DataSource

private const val DAY_STR_TIME = "strftime('%Y-%m-%d', cl.date/1000, 'unixepoch')"

@Dao
interface CallDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCallLogs(callLogs: List<CallLog>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertContacts(contacts: List<Contact>)

    @Query("SELECT * from contact WHERE suggested = 0")
    fun getUnSuggestedContacts(): DataSource.Factory<Int, Contact>

    @Query("SELECT * from contact WHERE suggested = 1 LIMIT 4")
    fun getSuggestedContacts(): Flow<List<Contact>>

    @Query("SELECT avg(call_length) FROM (SELECT sum(duration) as call_length from call_log GROUP BY number)")
    fun getAvgDurationGroupByNumber(): Double?

    @Query("UPDATE contact SET suggested = 1 WHERE norm_number IN (SELECT number FROM  (SELECT number, sum(duration) as duration FROM call_log GROUP BY number) WHERE duration BETWEEN:lower AND :upper ORDER BY duration ASC LIMIT :limit)")
    fun markSuggestedContacts(lower: Double, upper: Double, limit: Int = 4)

    @Query("SELECT cl.number as number, $DAY_STR_TIME as day, sum(cl.duration) as duration FROM call_log cl WHERE number = :number GROUP BY day ORDER BY day DESC")
    suspend fun getCallLogsGroupedByDay(number: String): List<HomeGraphModel>
}