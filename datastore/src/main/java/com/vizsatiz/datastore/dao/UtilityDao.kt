package com.vizsatiz.datastore.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vizsatiz.datastore.models.State

@Dao
interface UtilityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertState(ownerName: State)

    @Query("SELECT * FROM State WHERE owner_name = :ownerName")
    fun getStateBy(ownerName: String): State?

}