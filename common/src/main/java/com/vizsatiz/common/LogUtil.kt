package com.vizsatiz.common

import android.util.Log

object LogUtil {
    fun d(log: String, tag: String = "Closely") {
        Log.d(tag, log)
    }
}