package com.vizsatiz.common.platform

interface EnvironmentUtil {
    fun isDebugFeaturesEnabled(): Boolean
    fun isDataMocksEnabled(): Boolean
}