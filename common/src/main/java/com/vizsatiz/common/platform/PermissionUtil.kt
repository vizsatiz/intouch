package com.vizsatiz.common.platform

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

object PermissionUtil {

    const val PERMISSION_REQUEST_CONTACT_AND_CALL_READ = 900

    fun hasCallLogReadConsent(context: Context) = ContextCompat
        .checkSelfPermission(context, android.Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED

    fun hasContactReadConsent(context: Context) = ContextCompat
        .checkSelfPermission(context, android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED

    fun hasCallAncContactConsent(context: Context) = hasCallLogReadConsent(context) && hasContactReadConsent(context)
}