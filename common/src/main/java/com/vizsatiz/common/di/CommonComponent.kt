package com.vizsatiz.common.di

import android.content.Context
import com.vizsatiz.common.di.module.AppStateModule
import com.vizsatiz.common.di.module.CommonModule
import com.vizsatiz.common.di.module.CoroutineModule
import com.vizsatiz.common.di.module.PlatformModule
import com.vizsatiz.common.platform.EnvironmentUtil
import com.vizsatiz.common.state.AppStateUtil
import com.vizsatiz.datastore.di.component.DataStoreComponent
import dagger.Component
import javax.inject.Named
import kotlin.coroutines.CoroutineContext

@Component(
    modules = [
        CoroutineModule::class,
        AppStateModule::class,
        CommonModule::class,
        PlatformModule::class
    ],
    dependencies = [
        DataStoreComponent::class
    ]
)
@CommonScope
interface CommonComponent {

    @Named("IO") fun ioCoroutineContext(): CoroutineContext
    @Named("Default") fun asyncCoroutineContext(): CoroutineContext
    @Named("UI") fun uiCoroutineContext(): CoroutineContext

    fun getAppStateUtils(): AppStateUtil
    fun getApplicationContext(): Context
    fun getEnvironmentUtil(): EnvironmentUtil
}