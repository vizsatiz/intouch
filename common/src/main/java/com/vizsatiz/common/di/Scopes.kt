package com.vizsatiz.common.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
internal annotation class CommonScope