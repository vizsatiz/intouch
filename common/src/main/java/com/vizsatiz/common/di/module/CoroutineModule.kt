package com.vizsatiz.common.di.module

import com.vizsatiz.common.di.CommonScope
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import javax.inject.Named
import kotlin.coroutines.CoroutineContext

@Module
class CoroutineModule {

    @Provides
    @CommonScope
    @Named("IO")
    fun provideIOCoroutine(): CoroutineContext = Dispatchers.IO

    @Provides
    @CommonScope
    @Named("UI")
    fun provideUICoroutine(): CoroutineContext = Dispatchers.Main

    @Provides
    @CommonScope
    @Named("Default")
    fun provideDefaultCoroutine(): CoroutineContext = Dispatchers.Default
}