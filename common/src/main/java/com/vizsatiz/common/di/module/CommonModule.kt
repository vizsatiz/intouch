package com.vizsatiz.common.di.module

import android.content.Context
import com.vizsatiz.common.di.CommonScope
import dagger.Module
import dagger.Provides

@Module
class CommonModule(private val appContext: Context) {

    @Provides
    @CommonScope
    fun getAppContext(): Context = appContext
}