package com.vizsatiz.common.di.module

import com.vizsatiz.common.di.CommonScope
import com.vizsatiz.common.platform.EnvironmentUtil
import dagger.Module
import dagger.Provides

@Module
class PlatformModule(private val environmentUtils: EnvironmentUtil) {

    @Provides
    @CommonScope
    fun getEnvironmentUtil(): EnvironmentUtil = environmentUtils
}