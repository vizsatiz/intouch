package com.vizsatiz.common.di.module

import com.vizsatiz.common.di.CommonScope
import com.vizsatiz.common.state.AppStateUtil
import com.vizsatiz.common.state.AppStateUtilImpl
import dagger.Binds
import dagger.Module

@Module
interface AppStateModule {

    @CommonScope
    @Binds
    fun getAppStateUtils(appStateUtil: AppStateUtilImpl): AppStateUtil

}