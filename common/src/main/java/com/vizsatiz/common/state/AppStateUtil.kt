package com.vizsatiz.common.state

import com.vizsatiz.datastore.dao.UtilityDao
import com.vizsatiz.datastore.models.State
import java.util.Date
import javax.inject.Inject

interface AppStateUtil {

    suspend fun getStateByOwner(ownerName: String): State

    suspend fun updateStateByOwner(ownerName: String, value: String)

    companion object {
        const val STATE_OWNER_CALL_LOG_SYNC = "call_log_sync"
        const val STATE_OWNER_CONTACT_SYNC = "contacts_sync"
    }

}


class AppStateUtilImpl @Inject constructor(private val utilityDao: UtilityDao): AppStateUtil {

    override suspend fun getStateByOwner(ownerName: String): State = utilityDao
        .getStateBy(ownerName) ?: State(ownerName, Date(0).time.toString())

    override suspend fun updateStateByOwner(ownerName: String, value: String) = utilityDao
        .insertState(State(ownerName, value))

}